## Reporte RentAdvisor

### Reporte 15 marzo a 31 marzo
[Abrir tabla de efectivas](https://docs.google.com/spreadsheets/d/1nsqrvglCZBfaBVKE_qTlRUzw6urDxDUlKQ6icLvlUZE/pubhtml)

- **Total:** 120
- **Efectivas:** 48

### Efectivas

- **Asesores:** 1
- **Cliente:** 41
- **Pagina:** 2
- **Adwords:** 1
- **Referido:** 3

### Reporte 31 marzo a 15 abril

[Abrir tabla de efectivas](https://docs.google.com/spreadsheets/d/1BVjVSfrW3SApfH6gA4UBnUgrZsVmoeQLiTm91ZAzqk8/pubhtml)

- **Total:** 222
- **Efectivas:** 98

### Efectivas

- **Asesores:** 4
- **Cliente:** 75
- **Pagina:** 2
- **Adwords:** 17


### 15 marzo a 15 abril
[Abrir Tabla de efectivas](https://docs.google.com/spreadsheets/d/1hutuYmLR8yOvnM4bMwNj0uZOXf4l0irL1YC_0s3OjJc/pubhtml)

- **Total:** 342
- **Efectivas:** 146

### Efectivas

- **Asesores:** 5
- **Cliente:** 116
- **Pagina:** 4
- **Adwords:** 18
- **Referido:** 3

### Pagina

- **contactos:** 31
